﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AdventureWorks.DbModel;
using Serilog;
using Serilog.Sinks.File;

namespace AdventureWorks.Web.Controllers
{
	/// <summary>
	/// ProductsController
	/// </summary>
	public class ProductsController : ApiController
	{
		private readonly DbModel.Entities _entities = new DbModel.Entities();

		/// <summary>
		/// ProductsController constructor
		/// </summary>
		public ProductsController()
		{
			Log.Logger = new LoggerConfiguration()
				.MinimumLevel.Debug()
				// .WriteTo.Console()
				.WriteTo.File("logs\\adventureWorks.txt", rollingInterval: RollingInterval.Day)
				.CreateLogger();

			Log.Information("Hello, Maxim!");
		}

		/// <summary>
		/// Get all products
		/// </summary>
		/// <remarks>
		/// Get a list of all products
		/// </remarks>
		/// <returns></returns>
		/// <response code="200"></response>
		[ResponseType(typeof(IEnumerable<Product>))]
		public IQueryable<Product> Get()
		{
			return _entities.Products;
		}

		/// <summary>
		/// Put product
		/// </summary>
		/// <returns></returns>	
		[HttpPut]
		[ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
				Log.Error($"{product} is invalid");
				return BadRequest(ModelState);
            }

            if (id != product.ProductID)
            {
				Log.Error($"{product} is invalid");
				return BadRequest();
            }

			_entities.Entry(product).State = EntityState.Modified;

            try
            {
				_entities.SaveChanges();
				Log.Information($"{product} was successfully updated");
			}
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
					Log.Error($"{product} is not found");
					return NotFound();
                }
                else
                {
                    throw;
                }
            }

			
			return StatusCode(HttpStatusCode.OK);
        }

		/// <summary>
		/// Post new product
		/// </summary>
		/// <returns></returns>	
		[HttpPost]
		[ResponseType(typeof(Product))]
        public IHttpActionResult Post(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

			_entities.Products.Add(product);
			_entities.SaveChanges();

			Log.Information($"{product} was successfully added");
			return CreatedAtRoute("DefaultApi", new { id = product.ProductID }, product);
        }

		/// <summary>
		/// Delete product
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpDelete]
		[ResponseType(typeof(Product))]
        public IHttpActionResult Delete(int id)
        {
            Product product = _entities.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

			_entities.Products.Remove(product);
			_entities.SaveChanges();
			Log.Information($"Product with {id} was successfully deleted");
			return Ok(product);
        }

		/// <summary>
		/// Dispose entity
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
				_entities.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return _entities.Products.Count(e => e.ProductID == id) > 0;
        }
    }
}