﻿using System.Web.Mvc;

namespace AdventureWorks.Web.Controllers
{
	/// <summary>
	/// HomeController
	/// </summary>
	public class HomeController : Controller
    {
		/// <summary>
		/// Index
		/// </summary>
		/// <returns></returns>
		public ActionResult Index()
        {
            return View();
        }
    }
}