﻿using System.Web.Mvc;
using AdventureWorks.Services.HumanResources;
using Microsoft.ApplicationInsights;

namespace AdventureWorks.Web.Controllers
{
	/// <summary>
	/// DepartmentsController
	/// </summary>
	public class DepartmentsController : Controller
    {
		private readonly TelemetryClient telemetryClient = new TelemetryClient();
		// GET: Departments
		/// <summary>
		/// Index
		/// </summary>
		/// <returns></returns>
		public ActionResult Index()
        {
			this.telemetryClient.TrackEvent("User clicks link Department/id");
			DepartmentService departmentService = new DepartmentService();
            var departmentGroups = departmentService.GetDepartments();
			this.telemetryClient.TrackPageView("Department is loaded");
			return View(departmentGroups);
        }

		/// <summary>
		/// Employees
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		// GET: Departments/Employees/{id}
		public ActionResult Employees(int id)
        {
			DepartmentService departmentService = new DepartmentService();
            var departmentEmployees = departmentService.GetDepartmentEmployees(id);
            var departmentInfo = departmentService.GetDepartmentInfo(id);

            ViewBag.Title = "Employees in " + departmentInfo.Name + " Department";
			this.telemetryClient.TrackPageView("Department/id is loaded");
			return View(departmentEmployees);
        }
    }
}
