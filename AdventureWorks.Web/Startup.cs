﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdventureWorks.Web.Startup))]
namespace AdventureWorks.Web
{
    public partial class Startup
    {
		/// <summary>
		/// Configuration
		/// </summary>
		/// <param name="app"></param>
		public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
