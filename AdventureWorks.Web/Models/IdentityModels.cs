﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AdventureWorks.Web.Models
{
	/// <summary>
	/// ApplicationUser
	/// </summary>
	// You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
	public class ApplicationUser : IdentityUser
    {
		/// <summary>
		/// GenerateUserIdentityAsync
		/// </summary>
		/// <param name="manager"></param>
		/// <returns></returns>
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
	/// <summary>
	/// ApplicationDbContext
	/// </summary>
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
		/// <summary>
		/// ApplicationDbContext
		/// </summary>
		public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

		/// <summary>
		/// Create
		/// </summary>
		/// <returns></returns>
		public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}