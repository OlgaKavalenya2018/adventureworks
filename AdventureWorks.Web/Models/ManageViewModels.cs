﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace AdventureWorks.Web.Models
{
	/// <summary>
	/// IndexViewModel
	/// </summary>
	public class IndexViewModel
    {
		/// <summary>
		/// HasPassword
		/// </summary>
		public bool HasPassword { get; set; }
		/// <summary>
		/// Logins
		/// </summary>
		public IList<UserLoginInfo> Logins { get; set; }
		/// <summary>
		/// PhoneNumber
		/// </summary>
		public string PhoneNumber { get; set; }
		/// <summary>
		/// TwoFactor
		/// </summary>
		public bool TwoFactor { get; set; }
		/// <summary>
		/// BrowserRemembered
		/// </summary>
		public bool BrowserRemembered { get; set; }
    }

	/// <summary>
	/// ManageLoginsViewModel
	/// </summary>
	public class ManageLoginsViewModel
    {

		/// <summary>
		/// CurrentLogins
		/// </summary>
		public IList<UserLoginInfo> CurrentLogins { get; set; }
		/// <summary>
		/// OtherLogins
		/// </summary>
		public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

	/// <summary>
	/// FactorViewModel
	/// </summary>
	public class FactorViewModel
    {
		/// <summary>
		/// Purpose
		/// </summary>
		public string Purpose { get; set; }
    }

	/// <summary>
	/// SetPasswordViewModel
	/// </summary>
	public class SetPasswordViewModel
    {
		/// <summary>
		/// NewPassword
		/// </summary>
		[Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

		/// <summary>
		/// ConfirmPassword
		/// </summary>
		[DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

	/// <summary>
	/// ChangePasswordViewModel
	/// </summary>
	public class ChangePasswordViewModel
    {
		/// <summary>
		/// OldPassword
		/// </summary>
		[Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

		/// <summary>
		/// 
		/// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

		/// <summary>
		/// ConfirmPassword
		/// </summary>
		[DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

	/// <summary>
	/// AddPhoneNumberViewModel
	/// </summary>
	public class AddPhoneNumberViewModel
    {
		/// <summary>
		/// Number
		/// </summary>
		[Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

	/// <summary>
	/// VerifyPhoneNumberViewModel
	/// </summary>
	public class VerifyPhoneNumberViewModel
    {
		/// <summary>
		/// Code
		/// </summary>
		[Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

		/// <summary>
		/// PhoneNumber
		/// </summary>
		[Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

	/// <summary>
	/// ConfigureTwoFactorViewModel
	/// </summary>
	public class ConfigureTwoFactorViewModel
    {
		/// <summary>
		/// SelectedProvider
		/// </summary>
		public string SelectedProvider { get; set; }
		/// <summary>
		/// Providers
		/// </summary>
		public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }
}