﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AdventureWorks.Web.Models
{
	/// <summary>
	/// ExternalLoginConfirmationViewModel
	/// </summary>
	public class ExternalLoginConfirmationViewModel
    {
		/// <summary>
		/// Email
		/// </summary>
		[Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

	/// <summary>
	/// ExternalLoginListViewModel
	/// </summary>
	public class ExternalLoginListViewModel
    {
		/// <summary>
		/// ReturnUrl
		/// </summary>
		public string ReturnUrl { get; set; }
    }

	/// <summary>
	/// SendCodeViewModel
	/// </summary>
	public class SendCodeViewModel
    {
		/// <summary>
		/// SelectedProvider
		/// </summary>
		public string SelectedProvider { get; set; }
		/// <summary>
		/// Providers
		/// </summary>
		public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
		/// <summary>
		/// ReturnUrl
		/// </summary>
		public string ReturnUrl { get; set; }
		/// <summary>
		/// RememberMe
		/// </summary>
		public bool RememberMe { get; set; }
    }

	/// <summary>
	/// VerifyCodeViewModel
	/// </summary>
	public class VerifyCodeViewModel
    {
		/// <summary>
		/// Provider
		/// </summary>
		[Required]
        public string Provider { get; set; }

		/// <summary>
		/// Code
		/// </summary>
		[Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
		/// <summary>
		/// ReturnUrl
		/// </summary>
		public string ReturnUrl { get; set; }

		/// <summary>
		/// RememberBrowser
		/// </summary>
		[Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

		/// <summary>
		/// RememberMe
		/// </summary>
		public bool RememberMe { get; set; }
    }

	/// <summary>
	/// ForgotViewModel
	/// </summary>
	public class ForgotViewModel
    {
		/// <summary>
		/// Email
		/// </summary>
		[Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

	/// <summary>
	/// LoginViewModel
	/// </summary>
	public class LoginViewModel
    {
		/// <summary>
		/// Email
		/// </summary>
		[Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

		/// <summary>
		/// Password
		/// </summary>
		[Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

		/// <summary>
		/// RememberMe
		/// </summary>
		[Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

	/// <summary>
	/// RegisterViewModel
	/// </summary>
	public class RegisterViewModel
    {
		/// <summary>
		/// Email
		/// </summary>
		[Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

		/// <summary>
		/// Password
		/// </summary>
		[Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

		/// <summary>
		/// ConfirmPassword
		/// </summary>
		[DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

	/// <summary>
	/// ResetPasswordViewModel
	/// </summary>
	public class ResetPasswordViewModel
    {
		/// <summary>
		/// Email
		/// </summary>
		[Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

		/// <summary>
		/// Password
		/// </summary>
		[Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

		/// <summary>
		/// ConfirmPassword
		/// </summary>
		[DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

		/// <summary>
		/// Code
		/// </summary>
		public string Code { get; set; }
    }

	/// <summary>
	/// ForgotPasswordViewModel
	/// </summary>
	public class ForgotPasswordViewModel
    {
		/// <summary>
		/// Email
		/// </summary>
		[Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
